package com.android.commands.fireflyapi;




import android.util.Log;
import android.os.RemoteException;
import android.net.IEthernetManager;
import android.os.ServiceManager;
import android.os.RemoteException;

import android.os.storage.IMountService;

import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.StaticIpConfiguration;
import android.text.TextUtils;

import java.io.File;



import java.io.IOException;
public class FireflyApi{
    private static final String TAG = "FireflyApi";
    private String[] mArgs;
    private int mNextArg;
 

    IEthernetManager mEthM;
    IMountService mMountService;
    private static final String FIREFLY_NOT_RUNNING_ERR =
        "Error: Could not access the FireflyAPi.  Is the system running?";


    public static void main(String[] args) {
        int exitCode = 1;
        try {
            exitCode = new FireflyApi().run(args);
        } catch (Exception e) {
            Log.e(TAG, "Error", e);
            System.err.println("Error: " + e);
            if (e instanceof RemoteException) {
                System.err.println(FIREFLY_NOT_RUNNING_ERR);
            }
        }
        System.exit(exitCode);
    }

    public int run(String[] args) throws IOException, RemoteException {
        boolean validCommand = false;
        if (args.length < 1) {
            return showUsage();
        }
        mEthM = IEthernetManager.Stub.asInterface(ServiceManager.getService("ethernet"));

        mMountService = IMountService.Stub.asInterface(ServiceManager.getService("mount"));
        // mUm = IUserManager.Stub.asInterface(ServiceManager.getService("user"));
        // mPm = IPackageManager.Stub.asInterface(ServiceManager.getService("package"));
        // if (mPm == null) {
        //     System.err.println(PM_NOT_RUNNING_ERR);
        //     return 1;
        // }
        // mInstaller = mPm.getPackageInstaller();

        mArgs = args;
        String op = args[0];
        mNextArg = 1;
        Log.e(TAG, "op:"+op);

        for(int i=0;i<args.length;i++){
            Log.e(TAG, "args["+i+"]:"+args[i]);
            
        }

        if ("setEthIPAddress".equals(op)) {
            if(args.length < 2) return 1;

            boolean useStatic = args[1].equals("1");

            if(useStatic)
            {
                if(args.length == 7){
                    //settings put system ethernet_use_static_ip 1
                    //fireflyapi setEthIPAddress 0 168.168.100.245 255.255.0.0 168.168.0.1 202.96.128.86 202.96.128.166

                    String ipaddress = args[2];
                    String netmask = args[3];
                    String gateway = args[4];
                    String dns1 = args[5];
                    String dns2 = args[6];
                    // Log.v(TAG,"setEthIPAddress useStatic:"+useStatic+",ipaddress:"+ipaddress+",netmask:"+netmask
                    //     +",gateway:"+gateway+",dns1:"+dns1+",dns2:"+dns2);
                    IpConfiguration mIpConfiguration = NetworkUtils.getStaticIpConfiguration(ipaddress, netmask, gateway, dns1, dns2);
                    mEthM.setConfiguration(mIpConfiguration);
                    return 0;
                }else{
                    return 1;
                }
            }else{
                mEthM.setConfiguration(new IpConfiguration(IpAssignment.DHCP, ProxySettings.NONE,null,null));
                return 0;
            }


            
        }

        if("setEthernetEnabled".equals(op)){
            if(args.length == 2){
                boolean enabled = args[1].equals("1");
                boolean success = mEthM.setEthernetEnabled(enabled);
                //Log.v(TAG,"mEthM.setEthernetEnabled(enabled):"+success);
                System.err.println(success?0:1);
                return 0;
            }else{
               return 1; 
            }
        }

        if("getEthernetConnectState".equals(op)){
            int state = mEthM.getEthernetConnectState();
            //Log.v(TAG,"getEthernetConnectState:"+state);
            System.err.println(state);
            return 0; 
        }

        if("doUnmountVolume".equals(op)){
            //doUnmountVolume(String path,boolean force,boolean removeEncryption)
            //fireflyapi doUnmountVolume /mnt/usb_storage/USB_DISK2 1 0
            if(args.length == 4){
                String path = args[1];
                boolean force = args[2].equals("1");
                boolean removeEncryption = args[3].equals("1");
                //Log.v(TAG,"doUnmountVolume path:"+path+",force:"+force+",removeEncryption:"+removeEncryption);
                try {
                    mMountService.unmountVolume(path,force,removeEncryption);
                    System.err.println(0);
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.err.println(1);
                }

                return 0;
            }else{
               return 1; 
            }
        }

        if("getVolumeState".equals(op)){
            //fireflyapi getVolumeState /mnt/usb_storage/USB_DISK2 
            if(args.length == 2){
                String path = args[1];
                try {
                   
                    System.err.println( mMountService.getVolumeState(path) );
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return 0;
            }else{
               return 1; 
            }
        }

        if("installPackage".equals(op)){
            //fireflyapi installPackage /mnt/sdcard/update.zip
            if(args.length == 2){
                String path = args[1];
                if(!TextUtils.isEmpty(path)){   
                    path = new File(path).getAbsolutePath();
                    if(path.startsWith("/mnt/sdcard/")){
                        path = path.replace("/mnt/sdcard/", "/data/media/0/");
                    }else if(path.startsWith("/sdcard/")){
                        path = path.replace("/sdcard/", "/data/media/0/");
                    }
                    //Log.v(TAG,"installPackage :"+new File(path).getAbsolutePath());
                    try {
                        RecoverySystem.installPackage(new File(path));
                        return 0;
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        return 1;
                    }
                }else{
                    return 1;
                }
            

            }else{
                return 1; 
            }
        }
        

        return 1;
        
    }


    private static int showUsage() {
        System.err.println("usage: fireflyapi setEthIPAddress <useStatic> <ipaddress> <netmask> <gateway> <dns1> <dns2>");
        System.err.println("       (exapmle:fireflyapi setEthIPAddress 0 168.168.100.245 255.255.0.0 168.168.0.1 202.96.128.86 202.96.128.166)");
        System.err.println("       fireflyapi setEthernetEnabled [1/enable] [2/disable]");
        System.err.println("       fireflyapi doUnmountVolume <path> <force(1/0)> <removeEncryption(1/0)>");
        System.err.println("");
        return 1;
    }


 
}
